<?php
return array(
    'title' => __('WittyPress Option Panel', 'vp_textdomain'),
    'logo'  => 'http://www.wittycookie.com/img/index_logo.png',
    'menus' => array(
        array(
            'title' => __('General Options', 'vp_textdomain'),
            'name' => 'wittpress_1',
            'icon' => '',
            'controls' => array(
                array(
                    'type' => 'textbox',
                    'name' => 'facebook',
                    'label' => __('Facebook Page', 'vp_textdomain'),
                    'description' => __('Enter URL here.', 'vp_textdomain'),
                    'default' => 'abcdefg',
                    'validation' => 'url',
                ),
                array(
                    'type' => 'textbox',
                    'name' => 'pinteret',
                    'label' => __('Pinterest', 'vp_textdomain'),
                    'description' => __('Enter URL here.', 'vp_textdomain'),
                    'default' => 'abcdefg',
                    'validation' => 'url',
                ),
                array(
                    'type' => 'textbox',
                    'name' => 'google',
                    'label' => __('Google+ Page', 'vp_textdomain'),
                    'description' => __('Enter URL here.', 'vp_textdomain'),
                    'default' => 'abcdefg',
                    'validation' => 'url',
                ),
                array(
                    'type' => 'textbox',
                    'name' => 'meetup',
                    'label' => __('Meetup URLS', 'vp_textdomain'),
                    'description' => __('Enter URL here.', 'vp_textdomain'),
                    'default' => 'abcdefg',
                    'validation' => 'url',
                ),
            ),
        ),
    )
);
?>
